<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => ''], function () {
    Route::get('/', function () {
       return redirect()->route('restaurant');
    });

    Route::get('/product-search', function () {
        return view('search');
    });
});


Route::group(['prefix' => ''], function () {
    Route::get('/init-data', 'HomeController@initData')->name('init.data');

    Route::get('/restaurant', 'HomeController@restaurant')->name('restaurant');
});



