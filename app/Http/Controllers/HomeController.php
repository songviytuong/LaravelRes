<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Events\TestEvent;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function initData(){

        $data = [];
        $data['name'] = 'Lipit';
        event(new TestEvent($data));
        return response()->json($data);
    }

    public function restaurant()
    {
        return view('restaurant');
    }
}
