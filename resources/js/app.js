require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';

// import VeeValidate from 'vee-validate';
// import VueRouter from 'vue-router';

// Vue.use(VueRouter);
// Vue.use(VeeValidate, {
//     locale: 'ja'
// });

// const routes = [{
//         path: '/',
//         component: Survey
//     },
//     {
//         path: '/stock',
//         component: Stock
//     },
//     {
//         path: '/simulation',
//         component: Simulation
//     },
//     {
//         path: '/result',
//         component: Result
//     },

//     // for development
//     {
//         path: '/save',
//         component: Save
//     },
// ];

// const router = new VueRouter({
//     routes,
// });

// router.beforeEach((to, from, next) => {
//     if (to.path === '/' || to.path === '/stock' || to.path === '/simulation') {
//         resetPrintStyle();
//     }
//     next();
// });

// new Vue({
//     el: '#app',
//     router,
// });


Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        c_number: 0,
        c_data_products: [{
                'p_id': 1,
                'p_name': 'Product 01',
                'p_qty': 1,
                'p_price': 10,
            },
            {
                'p_id': 2,
                'p_name': 'Product 02',
                'p_qty': 1,
                'p_price': 10,
            },
            {
                'p_id': 3,
                'p_name': 'Product 03',
                'p_qty': 1,
                'p_price': 15,
            }
        ],
        message: "Hello Peace"
    },
    mutations: {
        increment(state) {
            state.c_number++
        }
    }
})

import App from './App.vue'
const app = new Vue({
    el: '#app',
    store,
    render: h => h(App)
})

import Sidebar from './components/SidebarComponent.vue'
const sidebar = new Vue({
    el: '#sidebar',
    store,
    render: h => h(Sidebar)
})

import SHC from './components/ShoppingCartComponent.vue'
const shc_side_open = new Vue({
    el: '#shopping-cart',
    store,
    render: h => h(SHC)
})

import DeliciousMenu from './components/DeliciousMenuComponent.vue'
const delicious_menu = new Vue({
    el: '#nav-tabs',
    store,
    render: h => h(DeliciousMenu)
})
