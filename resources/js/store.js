import debug from 'debug';

const store = {
  debug: true,
  state: {
    message: 'Hello from store',
    price: 1000000,
  },

  initData: {},
  tabsName:{
    '1':'Breakfast',
    '2':'Lunch',
    '3':'Dinner',
  }

};

export default store;
