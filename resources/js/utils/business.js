export default {
  filters: {
    getRound: function(value) {
      return Math.round(value * 100) / 100;
    },
    getRound2: function(value) {
      return Math.round(value);
    },
    getCeil: function(value) {
      return Math.ceil(value);
    },
    slugTab: function(value){
      return "#" + value.toLowerCase().trim() + "-dishes";
    },
    /**
     * format date_string to the format for estimate.
     * @param  {string} value example. 2018-01-11 04:12:50'
     * @return {string}
     */
    date_estimate_format: function(value) {
      const d = new Date(value);
      return `${d.getFullYear()}年${d.getMonth() + 1}月${d.getDate()}日`;
    },
    date_estimate_format2: function(value) {
      const d = new Date(value);
      return `${d.getFullYear()}年${d.getMonth() + 1}月`;
    },
    localeString: function(value) {
      return Number(value).toLocaleString();
    },
    localeString2: function(value) {
      return Number(value).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      });
    },
  },
}

